<?php
ini_set('display_errors', 1);
require_once __DIR__ . '/vendor/autoload.php';

$connection = new \App\Config\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);
$pdo = $connection->getPdo();


$var1 = htmlspecialchars($_POST['delete']);
$var2 = htmlspecialchars($_POST['reset']);

if (!empty($var1)) {
    App\Actions\deleteData($var1, $pdo);
} elseif (!empty($var2)) {
    App\Actions\resetData($var2, $pdo);
}
