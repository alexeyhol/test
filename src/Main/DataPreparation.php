<?php

namespace App\Main;

class DataPreparation
{
    private $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private $commonLink;
    private $creationDate;
    private $key;

    
    public function __construct($commonLink, $creationDate)
    {
        $this->chars;
        $this->commonLink = $commonLink;
        $this->creationDate = $creationDate;
    }


    /*Вспомогательные методы для формирования короткой ссылки*/

    private function creationKey()
    {
        $key = [];
        $maxCharsListIndex = strlen($this->chars) - 1;
        $maxKeyLength = 5;

        for ($i = 0; $i <= $maxKeyLength; $i++) {
            $key[] = $this->chars[mt_rand(0, $maxCharsListIndex)];
        }
 
        return $this->key = '-' . implode($key);
    }
    
    private function getHost()
    {
        $host = parse_url($this->commonLink);
        return $host['host'];
    }
    
    private function getScheme()
    {
        $scheme = parse_url($this->commonLink);
        return $scheme['scheme'];
    }

    /**************************************************************/
    
    public function getCommonLink()
    {
        return $this->commonLink;
    }
    
    public function getLinkKey()
    {
        return $this->key;
    }
    
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    public function getShortLink()
    {
        return $this->getScheme() . '://' . $this->getHost() . '/' . $this->creationKey();
    }
}
