<?php

/*Обратная трансляция ссылок и передаресация*/

namespace App\Main;

require_once __DIR__ . '/../../vendor/autoload.php';

class LinkBackCast
{
    private $link_key;
    
    public function __construct($key)
    {
        $this->link_key = $key;
    }

    public function getLinkFromDb($pdo)
    {
        $query = "SELECT `normal_url` FROM `data` WHERE `link_key` = ?";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array($this->link_key));
        $link = $stmt->fetch(\PDO::FETCH_COLUMN);
        return $link;
    }
    
    public function redirect($link)
    {
        header("Location: $link");
    }
}
