<?php

/*Соединение с базой данных*/

namespace App\Config;

use \PDO;

class DbConnection extends \PDO
{
    protected $pdo;
    
    private const OPTIONS = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false
    ];
    
    
    public function __construct($dbdriver, $host, $dbname, $charset, $username, $passwd)
    {
        $dsn = "$dbdriver:host=$host;dbname=$dbname;charset=$charset";
        $this->pdo = new PDO($dsn, $username, $passwd, self::OPTIONS);
    }
    
    public function getPdo()
    {
        return $this->pdo;
    }
}
