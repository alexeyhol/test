<?php

namespace App\Actions;

function loadData($normal_url, $short_url, $link_key, $date, $pdo)
{
    $query = "INSERT INTO `data` (`normal_url`, `short_url`, `link_key`, `date`) VALUES (?, ?, ?, ?)";
    $stmt = $pdo->prepare($query);
    $stmt->execute(array($normal_url, $short_url, $link_key, $date));
}
