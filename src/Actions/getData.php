<?php

/*Функции для получения данных*/

namespace App\Actions;

/*Получение текущей ссылки*/

function getCommonLink()
{
    return trim(htmlspecialchars($_POST['commonLink']));
}

/*Получение даты создания*/

function getCreationDate()
{
    return trim(htmlspecialchars($_POST['creationDate']));
}


/*Получение ключа ссылки*/

function getLinkKey()
{
    return trim(htmlspecialchars($_GET['key']));
}
