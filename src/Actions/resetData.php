<?php

namespace App\Actions;

/*Повторная генерация ссылки*/

function resetData($normal_url, $pdo)
{
    $data = new \App\Main\DataPreparation($normal_url, $date = date("H:i, d F Y"));
    $short_url = $data->getShortLink();
    $query = "UPDATE `data` SET `short_url` = ?, `date` = ? WHERE `normal_url` = ?";
    $stmt = $pdo->prepare($query);
    $stmt->execute(array($short_url, $date, $normal_url));
    header("Location: list.php");
}
