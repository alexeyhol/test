<?php

namespace App\Actions;

/*Удаление данных в таблице*/

function deleteData($short_url, $pdo)
{
    $query = "DELETE FROM `data` WHERE `short_url` = ?";
    $stmt = $pdo->prepare($query);
    $stmt->execute(array($short_url));
    header("Location: list.php");
}
