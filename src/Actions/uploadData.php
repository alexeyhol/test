<?php

/*Функции для выгрузки данных из базы и их вывода*/

namespace App\Actions;

/*Получение данных из базы*/

function uploadData($pdo)
{
    $query = "SELECT `id`, `normal_url`, `short_url`, `date` FROM `data`";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $data =  $stmt->fetchAll();
    return $data;
}

/*Печать полученных данных*/

function printData($data)
{
    echo '<table>';
    echo '<tr><td>ID</td><td>Normal Link</td><td>Short Link</td><td>Date of creation</td></tr>';
    foreach ($data as $value) {
        echo '<tr>';
        echo "<td class=\"id\">{$value['id']}</td>";
        echo "<td><a href=\"{$value['normal_url']}\" target=\"_blank\">{$value['normal_url']}</a></td>";
        echo "<td><a href=\"{$value['short_url']}\" target=\"_blank\">{$value['short_url']}</a></td>";
        echo "<td>{$value['date']}</td>";
        echo '</tr>';
    }
    echo '</table>';
}
