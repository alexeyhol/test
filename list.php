<?php
ini_set('display_errors', 1);
require_once __DIR__ . '/vendor/autoload.php';

use App\Actions;

$connection = new \App\Config\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);
$pdo = $connection->getPdo();

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>List of Links</title>
        <link rel="stylesheet" href="styles/main.css"/>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="twelve columns u-full-width main">
                    <h2>List of link's</h2>
                    
                       <?php Actions\printData(Actions\uploadData($pdo));?>
                    
                     <h2>Delete and Reset</h2>
                     
                     <form id="change" method="POST" action="change.php">
                         <div class="six columns">
                         <input type="text" name="delete" placeholder="input short link here">
                         <button type="submit" form="change">Delete</button>
                         </div>
                         <div class="six columns">
                         <input type="text" name="reset" placeholder="input normal link here">
                         <button type="submit" form="change">Re-generation</button>
                         </div>
                     </form>
                </div>
            </div>
        </div>
    </body>
</html>