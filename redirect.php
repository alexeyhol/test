<?php

require_once __DIR__ . '/vendor/autoload.php';

$connect = new \App\Config\DbConnection();
$pdo = $connect->getPdo();

$key = App\Actions\getLinkKey();

$link = new \App\Main\LinkBackCast($key);

$link->redirect($link->getLinkFromDb($pdo));
