<?php

ini_set('display_errors', 1);

require_once __DIR__ . '/vendor/autoload.php';

$connection = new \App\Config\DbConnection(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);
$pdo = $connection->getPdo();

$data = new App\Main\DataPreparation(App\Actions\getCommonLink(), App\Actions\getCreationDate());

App\Actions\loadData($data->getCommonLink(), $data->getShortLink(), $data->getLinkKey(), $data->getCreationDate(), $pdo);

//header("Location: {$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}/list.php");
header("Location: list.php");
exit;
