<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <link rel="stylesheet" href="styles/main.css"/>
    </head>
    <body>
        <div class="container">
            <biv class="row">
                <div class="twelve columns u-full-width main">
                    <h1>CREATING SHORT LINKS</h1>
                    <form action="action.php" method="POST">
                        <input type="hidden" name="creationDate" value="<?= date("H:i, d F Y") ?>">
                        <input type="text" name="commonLink">
                        <button type="submit">SHORT</button>
                    </form>
                </div>
            </biv>
        </div>
    </body>
</html>